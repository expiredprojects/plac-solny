﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	private static GameController game;

	public GameModel model;
	public GameView view;

	private Vector3 mouseVector;
	private DragEntity clone;

	void Start() {
		game = this;
		mouseVector = new Vector3(0.0f, 0.0f, 9.0f);
		model.currentPhase = Phase.START;
	}

	void OnGUI() {
		GUI.Label(new Rect(Screen.width * 0.05f, Screen.height * 0.1f, Screen.width * 0.1f, Screen.height * 0.1f), model.currentPhase.ToString());
		GUI.Label(new Rect(Screen.width * 0.05f, Screen.height * 0.2f, Screen.width * 0.1f, Screen.height * 0.1f), model.getTime().ToString());
		GUI.Label(new Rect(Screen.width * 0.05f, Screen.height * 0.3f, Screen.width * 0.2f, Screen.height * 0.1f), "Enter to Restart.");
	}

	void Update() {
		switch (model.currentPhase) {
		case Phase.START:
			StartGame();
			break;
		case Phase.IDLE:
			UpdateGame();
			break;
		case Phase.DRAG:
			UpdateGame();
			break;
		case Phase.END:
			EndGame();
			break;
		case Phase.NONE:
			UpdateInput();
			break;
		}
	}

	void StartGame() {
		Debug.Log("START");
		model.ResetTime();
		model.gameIsWon = false;
		view.getDesign().Redesign();
		view.CleanFlowers();
		model.currentPhase = Phase.IDLE;
	}

	void UpdateGame() {
		model.AdvanceTime(Time.deltaTime);
		if (model.getTime() < 0.0f) model.currentPhase = Phase.END;
	}

	void EndGame() {
		Debug.Log("END");
		model.currentPhase = Phase.NONE;
	}

	void UpdateInput() {
		if (Input.GetKeyUp(KeyCode.Return)) {
			model.currentPhase = Phase.START;
		}
	}

	public void InvokeVaseDown(FlowerColor flowerColor, Vector3 position) {
		if (model.currentPhase == Phase.IDLE) {
			view.MakeClone(flowerColor, position);
			clone = view.getClone();
			model.currentPhase = Phase.DRAG;
		}
	}
	
	public void InvokeVaseDrag() {
		if (model.currentPhase == Phase.DRAG) {
			mouseVector.x = Input.mousePosition.x;
			mouseVector.y = Input.mousePosition.y;
			if (clone) {
				clone.transform.position = getMouseInWorld();
			}
		}
	}
	
	public void InvokeVaseRelease() {
		if (clone) {
			if (model.currentPhase == Phase.DRAG) {
				if (view.getDesk().getBounds().Contains(getMouseInWorld())) {
					view.SpawnFlower(clone.getColor());
				}
				model.currentPhase = Phase.IDLE;
			}
			clone = null;
			view.DestroyClone();
		}
	}

	Vector3 getMouseInWorld() {
		return Camera.main.ScreenToWorldPoint(mouseVector);
	}

	public static GameController getController() {
		return game;
	}
}
