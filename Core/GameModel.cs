﻿using UnityEngine;
using System.Collections;

public class GameModel : MonoBehaviour {
	public static int designInit = 5;
	private const float timeInit = 15.0f;

	public Phase currentPhase = Phase.START;
	public bool gameIsWon = false;
	private float timeCurrent = timeInit;

	private int getDesign() {
		return designInit;
	}

	public float getTime() {
		return timeCurrent;
	}

	public void AdvanceTime(float deltaTime) {
		timeCurrent -= deltaTime;
	}

	public void ResetTime() {
		timeCurrent = timeInit;
	}
}
