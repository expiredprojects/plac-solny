﻿using UnityEngine;
using System.Collections;

public class GameView : MonoBehaviour {
	private DesignEntity design;
	private DeskEntity desk;
	private VaseEntity[] vases;
	//FLOWERS
	public FlowerEntity flowerSource;
	private FlowerEntity[] flowers;
	private int flowerIndex;
	//DRAG
	public DragEntity dragSource;
	private DragEntity clone;

	void Start () {
		design = transform.GetComponentInChildren<DesignEntity>();
		desk = transform.GetComponentInChildren<DeskEntity>();
		vases = transform.GetComponentsInChildren<VaseEntity>();
		DestroyClone();
		CleanFlowers();
	}

	public void MakeClone(FlowerColor flowerColor, Vector3 position) {
		if (!clone) {
			clone = Instantiate(dragSource, position, new Quaternion()) as DragEntity;
			clone.setColor(flowerColor);
		}
	}

	public void DestroyClone() {
		if (clone) {
			Destroy(clone.transform.root.gameObject);
			clone = null;
		}
	}

	public void SpawnFlower(FlowerColor flowerColor) {
		if (flowerIndex < flowers.Length) {
			FlowerEntity flower = Instantiate(flowerSource) as FlowerEntity;
			flower.setColor(flowerColor);
		
			int delta = 0;
			if (flowerIndex % 2.0f == 0.0f) {
				delta = -1 * flowerIndex / 2;
			}
			else {
				delta = Mathf.CeilToInt(flowerIndex / 2.0f);
			}

			flower.transform.position = new Vector3(0.5f + (float) delta, -0.75f, -1.0f);
			flowers[flowerIndex] = flower;
			flowerIndex++;
		}
	}

	public void CleanFlowers() {
		if (flowerIndex > 0) {
			foreach (FlowerEntity flower in flowers) {
				if (flower) {
					Destroy(flower.transform.root.gameObject);
				}
			}
		}
		flowers = new FlowerEntity[GameModel.designInit];
		flowerIndex = 0;
	}

	public DesignEntity getDesign() {
		return design;
	}

	public DeskEntity getDesk() {
		return desk;
	}
	
	public VaseEntity[] getVases() {
		return vases;
	}

	public DragEntity getClone() {
		return clone;
	}
}
