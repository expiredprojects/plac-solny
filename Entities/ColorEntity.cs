﻿using UnityEngine;
using System.Collections;

public class ColorEntity : MonoBehaviour {
	private FlowerColor flowerColor;	
	
	public void setColor(FlowerColor flowerColor) {
		SpriteRenderer flowerRenderer = GetComponent<SpriteRenderer>();
		this.flowerColor = flowerColor;
		switch (flowerColor) {
		case FlowerColor.BLUE:
			flowerRenderer.color = Color.blue;
			break;
		case FlowerColor.GREEN:
			flowerRenderer.color = Color.green;
			break;
		case FlowerColor.PINK:
			flowerRenderer.color = Color.magenta;
			break;
		case FlowerColor.RED:
			flowerRenderer.color = Color.red;
			break;
		case FlowerColor.SEA:
			flowerRenderer.color = Color.cyan;
			break;
		case FlowerColor.YELLOW:
			flowerRenderer.color = Color.yellow;
			break;
		}
	}
	
	public FlowerColor getColor() {
		return flowerColor;
	}	
}
