﻿using UnityEngine;
using System.Collections;

public class DecoEntity : ColorEntity {
	void Start () {
		VaseEntity vase = transform.parent.GetComponent<VaseEntity>();
		if (vase) {
			setColor(vase.flowerColor);
		}
	}
}
