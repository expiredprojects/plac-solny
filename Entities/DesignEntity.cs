﻿using UnityEngine;
using System.Collections;

public class DesignEntity : MonoBehaviour {
	private int designInit;
	private ColorEntity[] children;

	void Start() {
		designInit = GameModel.designInit;
		children = transform.GetComponentsInChildren<ColorEntity>();
		if (children.Length != designInit) {
			Debug.LogError("Synchronization error (children: " + children.Length + " design: " + designInit + ")");
		}
	}

	public void Redesign() {
		for (int i = 0; i < designInit; i++) {
			int random = Random.Range(0, 6);
			FlowerColor flowerColor;
			switch (random) {
			case 0:
				flowerColor = FlowerColor.BLUE;
				break;
			case 1:
				flowerColor = FlowerColor.GREEN;
				break;
			case 2:
				flowerColor = FlowerColor.PINK;
				break;
			case 3:
				flowerColor = FlowerColor.RED;
				break;
			case 4:
				flowerColor = FlowerColor.SEA;
				break;
			case 5:
				flowerColor = FlowerColor.YELLOW;
				break;
			default:
				flowerColor = FlowerColor.NONE;
				break;
			}
			children[i].setColor(flowerColor);
		}
	}
}
