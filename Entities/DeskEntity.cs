﻿using UnityEngine;
using System.Collections;

public class DeskEntity : MonoBehaviour {
	private BoxCollider boxCollider;

	void Start() {
		boxCollider = GetComponent<BoxCollider>();
	}

	public Bounds getBounds() {
		return boxCollider.bounds;
	}
}
