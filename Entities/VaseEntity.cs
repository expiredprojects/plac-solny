﻿using UnityEngine;
using System.Collections;

public class VaseEntity : MonoBehaviour {
	public FlowerColor flowerColor;

	void OnMouseDown() {
		GameController.getController().InvokeVaseDown(flowerColor, transform.position);
	}

	void OnMouseDrag() {
		GameController.getController().InvokeVaseDrag();
	}

	void OnMouseUp() {
		GameController.getController().InvokeVaseRelease();
	}
}
