﻿using UnityEngine;
using System.Collections;

public enum FlowerColor {
	NONE,
	RED, 
	BLUE,
	GREEN,
	YELLOW,
	PINK,
	SEA
}
