﻿using UnityEngine;
using System.Collections;

public enum Phase {
	NONE,
	START,
	IDLE,
	DRAG,
	END
}
